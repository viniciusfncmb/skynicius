package com.br.skynicius.backendapi.controller;

import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.br.skynicius.backendapi.service.SummaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("summary")
@Api(value = "Summary", description = "API to handle flight summaries", tags = { "Summary" })
public class SummaryController {

    private final SummaryService service;

    @Autowired
    public SummaryController(final SummaryService service) {
        this.service = service;
    }

    @ApiOperation(value = "Find all flight summaries")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<SummaryConsult> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "Delete all flight summaries")
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject deleteAll() {
        service.deleteALl();
        JSONObject jo = new JSONObject();
        jo.put("message", "Records successfully deleted.");
        return jo;
    }
}
