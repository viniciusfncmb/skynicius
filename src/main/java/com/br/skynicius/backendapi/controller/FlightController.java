package com.br.skynicius.backendapi.controller;

import com.br.skynicius.backendapi.exception.BusinessException;
import com.br.skynicius.backendapi.model.dto.FlightRequestDto;
import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.br.skynicius.backendapi.service.FlightService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("flight")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Parameters with invalid values.")
})
@Api(value = "Flights", description = "API to search flight information", tags = {"Flights"})
public class FlightController {

    private final FlightService flightService;

    @Autowired
    public FlightController(final FlightService flightService) {
        this.flightService = flightService;
    }

    @ApiOperation(value = "Returns average flight prices")
    @PostMapping(value = "/avg", produces = MediaType.APPLICATION_JSON_VALUE)
    public SummaryConsult getSummaryFlight(
            @ApiParam(value = "Destination Ariports", example = "OPO,LIS")
            @RequestParam("fly_to") String flyTo,

            @ApiParam(value = "Currency", example = "EUR")
            @RequestParam(value = "curr", required = false) String curr,

            @ApiParam(value = "Date From (yyyy/MM/dd)", example = "2020/01/01")
            @RequestParam(value = "date_from", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd") LocalDate dtFrom,

            @ApiParam(value = "Date To (yyyy/MM/dd)", example = "2020/06/01")
            @RequestParam(value = "date_to", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd") LocalDate dtTo) throws BusinessException {

        var reqParamDto = new FlightRequestDto(flyTo, curr, dtFrom, dtTo);
        return flightService.getSummaryFlight(reqParamDto);
    }


}
