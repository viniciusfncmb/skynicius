package com.br.skynicius.backendapi.exception;

public class BusinessException extends Exception{
    public BusinessException(String s) {
        super(s);
    }
}
