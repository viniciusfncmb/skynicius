package com.br.skynicius.backendapi.service;

import com.br.skynicius.backendapi.exception.BusinessException;
import com.br.skynicius.backendapi.model.dto.FlightRequestDto;
import com.br.skynicius.backendapi.model.entity.SummaryConsult;

public interface FlightService {

    SummaryConsult getSummaryFlight(final FlightRequestDto reqParamDto) throws BusinessException;
}
