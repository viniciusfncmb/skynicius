package com.br.skynicius.backendapi.service;

import com.br.skynicius.backendapi.exception.BusinessException;
import com.br.skynicius.backendapi.model.dto.*;
import com.br.skynicius.backendapi.model.entity.Airport;
import com.br.skynicius.backendapi.model.entity.SummaryAvgFlight;
import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.br.skynicius.backendapi.repository.AirportRepository;
import com.br.skynicius.backendapi.repository.SummaryConsultRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.format.DateTimeFormatter;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

@Service
public class FlightServiceImpl implements FlightService {

    @Value(value = "${url.api.skypicker}")
    private String urlSkypicker;

    @Value(value = "${default.fly.destinations}")
    private String defaultDestinations;

    @Value(value = "${default.fly.currency}")
    private String defaultCurrency;

    @Value(value = "${param.type.search.skypicker}")
    private String paramTypeSearch;

    private final RestTemplate restTemplate;

    private final AirportRepository airportRepository;

    private final SummaryConsultRepository summaryConsultRepository;

    private static final String PRICE_CURRENCY = "EUR";

    @Autowired
    public FlightServiceImpl(
            RestTemplate restTemplate,
            AirportRepository airportRepository,
            SummaryConsultRepository summaryConsultRepository
    ) {
        this.restTemplate = restTemplate;
        this.airportRepository = airportRepository;
        this.summaryConsultRepository = summaryConsultRepository;
    }

    @Caching(
            cacheable = {@Cacheable(value = "summaryFlight",
                    condition = "#reqParamDto.dtFrom.isPresent() && #reqParamDto.dtTo.isPresent()")},
            put = {@CachePut(value = "summaryFlight",
                    condition = "#reqParamDto.isRecentDate()")}
    )
    public SummaryConsult getSummaryFlight(final FlightRequestDto reqParamDto) throws BusinessException {

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(urlSkypicker)
                .queryParam("fly_to", reqParamDto.getFlyTo().orElse(defaultDestinations))
                .queryParam("partner", paramTypeSearch)
                .queryParam("curr", reqParamDto.getCurr().orElse(defaultCurrency));

        setDateParams(reqParamDto, uriBuilder);

        var resp = restTemplate.getForObject(uriBuilder.toUriString(), FlightResponseDto.class);

        return createSummaryConsult(Objects.requireNonNull(resp, "Error reading WS response"), reqParamDto);
    }

    private void setDateParams(final FlightRequestDto reqParamDto, UriComponentsBuilder uriBuilder) {
        var dtFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        reqParamDto.getDtFrom().ifPresent(localDate -> uriBuilder.queryParam("dt_form", localDate.format(dtFormatter)));
        reqParamDto.getDtTo().ifPresent(localDate -> uriBuilder.queryParam("dt_To", localDate.format(dtFormatter)));

    }

    private SummaryConsult createSummaryConsult(final FlightResponseDto respDto, final FlightRequestDto reqDto)
            throws BusinessException {

        List<DataDto> data = respDto.getData();
        if (data.isEmpty()) {
            throw new BusinessException("The Skypicker's API didn't return data for the reported parameters.");
        }

        SummaryConsult summaryConsult = new SummaryConsult();

        summaryConsult.setDestinations(reqDto.getFlyTo().orElse(defaultDestinations));
        summaryConsult.setCurrency(reqDto.getCurr().orElse(defaultCurrency));
        reqDto.getDtFrom().ifPresent(summaryConsult::setDtFrom);
        reqDto.getDtTo().ifPresent(summaryConsult::setDtTo);

        var destinations = reqDto.getFlyTo().orElse("").split(",");
        for (String dest : destinations) {
            SummaryAvgFlight summaryAvgFlight = createSummaryAvgFlight(data, dest);
            summaryConsult.getSummaryAvgFlights().add(summaryAvgFlight);
        }

        return summaryConsultRepository.save(summaryConsult);
    }

    private SummaryAvgFlight createSummaryAvgFlight(final List<DataDto> data, final String dest) {

        SummaryAvgFlight summaryAvgFlight = new SummaryAvgFlight();
        summaryAvgFlight.setCurrency(PRICE_CURRENCY);

        Optional<Airport> airport = airportRepository.findAirportByIata(dest);
        airport.ifPresent(arp -> summaryAvgFlight.setAirportName(arp.getName()));
        summaryAvgFlight.setAirportCode(dest);

        Predicate<DataDto> filterDest = x -> StringUtils.equals(dest, x.getFlyTo().orElse(null));

        //statics to flight prices
        ToDoubleFunction<DataDto> dbFunction = x -> x.getConversion().flatMap(ConversionDto::getEuroPrice).orElse(BigDecimal.ZERO).doubleValue();
        var statistics = getStatistics(data, filterDest, dbFunction);
        summaryAvgFlight.setAvgFlightPrices(BigDecimal.valueOf(statistics.getAverage()).setScale(2, RoundingMode.HALF_EVEN));
        summaryAvgFlight.setTotalFlights(statistics.getCount());

        //statics to bags 1 prices
        dbFunction = x -> x.getBagsPrice().flatMap(BagsPriceDto::getOne).orElse(BigDecimal.ZERO).doubleValue();
        statistics = getStatistics(data, filterDest, dbFunction);
        summaryAvgFlight.setAvgBagPrices1(BigDecimal.valueOf(statistics.getAverage()).setScale(2, RoundingMode.HALF_EVEN));

        //statics to bags 2 prices
        dbFunction = x -> x.getBagsPrice().flatMap(BagsPriceDto::getTwo).orElse(BigDecimal.ZERO).doubleValue();
        statistics = getStatistics(data, filterDest, dbFunction);
        summaryAvgFlight.setAvgBagPrices2(BigDecimal.valueOf(statistics.getAverage()).setScale(2, RoundingMode.HALF_EVEN));

        return summaryAvgFlight;
    }

    private DoubleSummaryStatistics getStatistics(List<DataDto> data, Predicate<DataDto> filterDest,
                                                  ToDoubleFunction<DataDto> dbFunction) {
        return data.stream()
                .filter(filterDest)
                .mapToDouble(dbFunction)
                .summaryStatistics();
    }


}
