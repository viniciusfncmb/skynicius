package com.br.skynicius.backendapi.service;

import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.br.skynicius.backendapi.repository.SummaryConsultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SummaryServiceImpl implements SummaryService{

    private final SummaryConsultRepository repository;

    @Autowired
    public SummaryServiceImpl(final SummaryConsultRepository repository){
        this.repository = repository;
    }

    @Override
    public Iterable<SummaryConsult> findAll() {
        return repository.findAll();
    }

    @Override
    public void deleteALl() {
        repository.deleteAll();
    }
}
