package com.br.skynicius.backendapi.service;

import com.br.skynicius.backendapi.model.entity.SummaryConsult;

public interface SummaryService {

    Iterable<SummaryConsult> findAll();
    void deleteALl();
}
