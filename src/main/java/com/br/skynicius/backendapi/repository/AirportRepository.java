package com.br.skynicius.backendapi.repository;

import com.br.skynicius.backendapi.model.entity.Airport;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface AirportRepository extends PagingAndSortingRepository<Airport, Long> {

    Optional<Airport> findAirportByIata(final String iata);
}
