package com.br.skynicius.backendapi.repository;

import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface SummaryConsultRepository extends PagingAndSortingRepository<SummaryConsult, Long> {
}
