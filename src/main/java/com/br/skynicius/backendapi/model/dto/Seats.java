package com.br.skynicius.backendapi.model.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Seats implements Serializable {

    private Integer passengers;
    private Integer adults;
    private Integer children;
    private Integer infants;
}
