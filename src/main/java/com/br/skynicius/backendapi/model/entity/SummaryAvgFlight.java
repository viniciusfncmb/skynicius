package com.br.skynicius.backendapi.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
public class SummaryAvgFlight implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String currency;
    private String airportCode;
    private String airportName;
    private Long totalFlights;
    private BigDecimal avgFlightPrices;
    private BigDecimal avgBagPrices1;
    private BigDecimal avgBagPrices2;

    @ManyToOne
    @JsonIgnore
    private SummaryConsult  summaryConsult;

}
