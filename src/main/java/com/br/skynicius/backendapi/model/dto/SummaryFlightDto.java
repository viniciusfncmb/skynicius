package com.br.skynicius.backendapi.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SummaryFlightDto {

    private BigDecimal avgFlightPrices;
    private BigDecimal avgBagPrices1;
    private BigDecimal avgBagPrices2;
    private String currency;
    private Date dtFrom;
    private Date dtTo;
    private String airportName;
}
