package com.br.skynicius.backendapi.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class SummaryConsult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate dtFrom;

    private LocalDate dtTo;

    private String destinations;

    private String currency;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<SummaryAvgFlight> summaryAvgFlights = new HashSet<>();
}
