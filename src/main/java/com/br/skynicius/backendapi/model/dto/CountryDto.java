package com.br.skynicius.backendapi.model.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CountryDto implements Serializable {

    private String code;
    private String name;
}
