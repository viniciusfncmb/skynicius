package com.br.skynicius.backendapi.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Optional;

@Data
public class BagsPriceDto implements Serializable {

    @JsonProperty("1")
    private BigDecimal one;

    @JsonProperty("2")
    private BigDecimal two;

    private BigDecimal hand;

    public Optional<BigDecimal> getOne() {
        return Optional.ofNullable(one);
    }

    public Optional<BigDecimal> getTwo() {
        return Optional.ofNullable(two);
    }
}
