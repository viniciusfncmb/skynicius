package com.br.skynicius.backendapi.model.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AvailabilityDto implements Serializable {

    private Integer seats;
}
