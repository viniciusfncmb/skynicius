package com.br.skynicius.backendapi.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Optional;

@Data
public class ConversionDto implements Serializable {

    private BigDecimal foreignPrice;

    private String foreignCurrencyCode;

    @JsonProperty("EUR")
    private BigDecimal euroPrice;

    public Optional<BigDecimal> getEuroPrice() {
        return Optional.ofNullable(euroPrice);
    }

    public Optional<String> getForeignCurrencyCode() {
        return Optional.ofNullable(foreignCurrencyCode);
    }
}
