package com.br.skynicius.backendapi.model.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
public class Airport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "IATA may not be empty")
    @Column(length = 3)
    private String iata;

    @NotEmpty(message = "ICAO may not be empty")
    @Column(length = 4)
    private String icao;

    @NotEmpty(message = "Name may not be empty")
    @Size(max = 150)
    private String name;

    @NotEmpty(message = "Location may not be empty")
    @Size(max = 150)
    private String location;
}
