package com.br.skynicius.backendapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Optional;

@Value
@AllArgsConstructor
public class FlightRequestDto implements Serializable {

    private String flyTo;
    private String curr;
    private LocalDate dtFrom;
    private LocalDate dtTo;

    public Optional<String> getFlyTo(){
        return Optional.ofNullable(flyTo);
    }

    public Optional<String> getCurr(){
        return Optional.ofNullable(curr);
    }

    public Optional<LocalDate> getDtFrom() {
        return Optional.ofNullable(dtFrom);
    }

    public Optional<LocalDate> getDtTo() {
        return Optional.ofNullable(dtTo);
    }

    public boolean isRecentDate(){
        return this.dtTo != null && (this.dtTo.equals(LocalDate.now()) || this.dtTo.isAfter(LocalDate.now()));
    }
}
