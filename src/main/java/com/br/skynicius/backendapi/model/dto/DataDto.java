package com.br.skynicius.backendapi.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
@JsonIgnoreProperties(value = {"tracking_pixel", "transfers", "found_on"})
public class DataDto implements Serializable {

    private String mapIdfrom;

    @JsonProperty("return_duration")
    private String returnDuration;

    private String flyTo;

    private ConversionDto conversion;

    private BigDecimal quality;

    @JsonProperty("deep_link")
    private String deepLink;

    private String mapIdto;

    private Integer nightsInDest;

    private String id;

    @JsonProperty("fly_duration")
    private String flyDuration;

    private CountryDto countryTo;

    @JsonProperty("bags_price")
    private BagsPriceDto bagsPrice;

    @JsonProperty("baglimit")
    private BagLimitDto bagLimit;

    @JsonProperty("aTimeUTC")
    private Date aTimeUTC;

    private BigDecimal distance;

    private BigDecimal price;

    @JsonProperty("type_flights")
    private List<String> typeFlights;

    private String cityTo;

    private String flyFrom;

    @JsonProperty("dTimeUTC")
    private Date dTimeUTC;

    private Integer p1;

    private Integer p2;

    private Integer p3;

    private CountryDto countryFrom;

    @JsonProperty("dTime")
    private Date dTime;

    @JsonProperty("booking_token")
    private String bookingToken;

    private String cityFrom;

    @JsonProperty("aTime")
    private Date aTime;

    @JsonProperty("virtual_interlining")
    private boolean virtualInterlining;

    @JsonProperty("throw_away_ticketing")
    private boolean throwAwayTicketing;

    private DurationDto duration;

    private String cityCodeFrom;

    private String cityCodeTo;

    private List<String[]> routes;

    private List<RouteDto> route;

    private String[] airlines;

    @JsonProperty("pnr_count")
    private Integer pnrCount;

    @JsonProperty("has_airport_change")
    private Boolean hasAirportChange;

    @JsonProperty("technical_stops")
    private Integer technicalStops;

    private AvailabilityDto availability;

    @JsonProperty("facilitated_booking_available")
    private Boolean facilitatedBookingAvailable;

    public Optional<String> getFlyTo() {
        return Optional.ofNullable(flyTo);
    }

    public Optional<ConversionDto> getConversion() {
        return Optional.ofNullable(conversion);
    }

    public Optional<BagsPriceDto> getBagsPrice() {
        return Optional.ofNullable(bagsPrice);
    }
}
