package com.br.skynicius.backendapi.interceptor;

import com.br.skynicius.backendapi.controller.FlightController;
import com.br.skynicius.backendapi.controller.SummaryController;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice(assignableTypes = {FlightController.class, SummaryController.class})
public class ResponseBodyHandler implements  ResponseBodyAdvice<Object> {


    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }


    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (o instanceof Exception) {
            return new ResponseEntity<>(
                    ((Exception) o).getMessage(),
                    HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(o, HttpStatus.OK);
    }

}
