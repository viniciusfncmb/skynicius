package com.br.skynicius.backendapi.model.entity.faker;

import com.br.skynicius.backendapi.model.entity.SummaryAvgFlight;
import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.github.javafaker.Faker;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class SummaryConsultFaker {

    private SummaryConsultFaker() {
    }

    public static SummaryConsult build() {
        Faker faker = Faker.instance();

        SummaryConsult summaryConsult = new SummaryConsult();
        summaryConsult.setId(faker.number().randomNumber());
        summaryConsult.setDestinations("OPO,LIS");
        summaryConsult.setCurrency(faker.currency().code());
        summaryConsult.setDtFrom(LocalDate.now().plusDays(-10));
        summaryConsult.setDtTo(LocalDate.now());
        summaryConsult.setSummaryAvgFlights(SummaryAvgFlightFaker.createFakeList());

        return summaryConsult;
    }
}
