package com.br.skynicius.backendapi.model.entity.faker;

import com.br.skynicius.backendapi.model.entity.SummaryAvgFlight;
import com.github.javafaker.Faker;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class SummaryAvgFlightFaker {

    private SummaryAvgFlightFaker(){}

    public static SummaryAvgFlight build(){
        Faker faker = Faker.instance();
        SummaryAvgFlight summaryAvgFlight = new SummaryAvgFlight();
        summaryAvgFlight.setId(faker.number().randomNumber());
        summaryAvgFlight.setAirportCode(faker.lorem().characters(3));
        summaryAvgFlight.setAirportName(faker.name().firstName());
        summaryAvgFlight.setAvgFlightPrices(BigDecimal.valueOf(faker.number().randomDouble(2, 0, 1000)));
        summaryAvgFlight.setAvgBagPrices1(BigDecimal.valueOf(faker.number().randomDouble(2, 0, 1000)));
        summaryAvgFlight.setAvgBagPrices2(BigDecimal.valueOf(faker.number().randomDouble(2, 0, 1000)));
        summaryAvgFlight.setCurrency(faker.currency().code());
        summaryAvgFlight.setTotalFlights(faker.number().randomNumber());
        return summaryAvgFlight;
    }

    public static Set<SummaryAvgFlight> createFakeList(){
        Faker faker = Faker.instance();

        Set<SummaryAvgFlight> summaryAvgFlights = new HashSet<>();
        for (int i = 0; i < faker.number().randomDigit() ; i++) {
            SummaryAvgFlight avgFaker = build();
            summaryAvgFlights.add(avgFaker);
        }
        return summaryAvgFlights;
    }
}
