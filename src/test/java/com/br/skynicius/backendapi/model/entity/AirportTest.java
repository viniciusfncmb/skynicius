package com.br.skynicius.backendapi.model.entity;

import com.br.skynicius.backendapi.model.entity.faker.AirportFaker;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class AirportTest {

    private Faker faker;
    private Airport airport;

    @BeforeEach
    public void init() {
        this.faker = Faker.instance();
        this.airport = AirportFaker.build();
    }

    @Test
    void getId() {
        assertNotNull(airport.getId());
    }

    @Test
    void getIata() {
        assertNotNull(airport.getIata());
    }

    @Test
    void getIcao() {
        assertNotNull(airport.getIcao());
    }

    @Test
    void getName() {
        assertNotNull(airport.getName());
    }

    @Test
    void getLocation() {
        assertNotNull(airport.getLocation());
    }

    @Test
    void setId() {
        Long id = faker.number().randomNumber();
        airport.setId(id);
        assertEquals(id, airport.getId());
    }

    @Test
    void setIata() {
        String iata = faker.lorem().characters(3);
        airport.setIata(iata);
        assertEquals(iata, airport.getIata());
    }

    @Test
    void setIcao() {
        String icao = faker.lorem().characters(4);
        airport.setIcao(icao);
        assertEquals(icao, airport.getIcao());
    }

    @Test
    void setName() {
        String name = faker.name().firstName();
        airport.setName(name);
        assertEquals(name, airport.getName());
    }

    @Test
    void setLocation() {
        String location = faker.address().cityName();
        airport.setLocation(location);
        assertEquals(location, airport.getLocation());
    }

    @Test
    void testEquals() {
        Airport airport2 = AirportFaker.build();
        assertNotEquals(airport, airport2);
    }

    @Test
    void canEqual() {
        Airport airport2 = AirportFaker.build();
        assertTrue(airport.canEqual(airport2));
    }

    @Test
    void testHashCode() {
        Airport airport2 = AirportFaker.build();
        assertNotEquals(airport.hashCode(), airport2.hashCode());
    }

    @Test
    void testToString() {
        String[] fields = {"id", "iata", "icao", "name", "location"};
        assertTrue(Stream.of(fields).allMatch(airport.toString()::contains));
    }
}