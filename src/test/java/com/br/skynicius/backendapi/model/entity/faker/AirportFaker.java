package com.br.skynicius.backendapi.model.entity.faker;

import com.br.skynicius.backendapi.model.entity.Airport;
import com.github.javafaker.Faker;
import lombok.Builder;

@Builder
public class AirportFaker{

    private AirportFaker(){}

    public static Airport build() {
        Faker faker = Faker.instance();
        Airport airport = new Airport();
        airport.setId(faker.number().randomNumber());
        airport.setIata(faker.lorem().characters(3));
        airport.setIcao(faker.lorem().characters(4));
        airport.setName(faker.name().name());
        airport.setLocation(faker.address().city());
        return airport;
    }

}
