package com.br.skynicius.backendapi.model.entity;

import com.br.skynicius.backendapi.model.entity.faker.SummaryAvgFlightFaker;
import com.br.skynicius.backendapi.model.entity.faker.SummaryConsultFaker;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SummaryConsultTest {

    private Faker faker;
    private SummaryConsult summaryConsult;

    @BeforeEach
    public void init() {
        this.faker = Faker.instance();
        this.summaryConsult = SummaryConsultFaker.build();
    }

    @Test
    void getId() {
        assertNotNull(summaryConsult.getId());
    }

    @Test
    void getDtFrom() {
        assertNotNull(summaryConsult.getDtFrom());
    }

    @Test
    void getDtTo() {
        assertNotNull(summaryConsult.getDtTo());
    }

    @Test
    void getDestinations() {
        assertNotNull(summaryConsult.getDestinations());
    }

    @Test
    void getCurrency() {
        assertNotNull(summaryConsult.getCurrency());
    }

    @Test
    void getSummaryAvgFlights() {
        assertNotNull(summaryConsult.getSummaryAvgFlights());
        assertFalse(summaryConsult.getSummaryAvgFlights().isEmpty());
    }

    @Test
    void setId() {
        Long id = faker.number().randomNumber();
        summaryConsult.setId(id);
        assertEquals(id, summaryConsult.getId());
    }

    @Test
    void setDtFrom() {
        LocalDate dtFrom = LocalDate.now();
        summaryConsult.setDtFrom(dtFrom);
        assertEquals(dtFrom, summaryConsult.getDtFrom());
    }

    @Test
    void setDtTo() {
        LocalDate dtTo = LocalDate.now();
        summaryConsult.setDtFrom(dtTo);
        assertEquals(dtTo, summaryConsult.getDtFrom());
    }

    @Test
    void setDestinations() {
        String destinations = "CHG,OPO";
        summaryConsult.setDestinations(destinations);
        assertEquals(destinations, summaryConsult.getDestinations());
    }

    @Test
    void setCurrency() {
        String currency = faker.currency().code();
        summaryConsult.setCurrency(currency);
        assertEquals(currency, summaryConsult.getCurrency());
    }

    @Test
    void setSummaryAvgFlights() {
        Set<SummaryAvgFlight> avgs = SummaryAvgFlightFaker.createFakeList();
        summaryConsult.setSummaryAvgFlights(avgs);
        assertEquals(avgs, summaryConsult.getSummaryAvgFlights());
    }

    @Test
    void testEquals() {
        SummaryConsult summaryConsult2 = SummaryConsultFaker.build();
        assertNotEquals(summaryConsult, summaryConsult2);
    }

    @Test
    void canEqual() {
        SummaryConsult summaryConsult2 = SummaryConsultFaker.build();
        assertTrue(summaryConsult.canEqual(summaryConsult2));
    }

    @Test
    void testHashCode() {
        SummaryConsult summaryConsult2 = SummaryConsultFaker.build();
        assertNotEquals(summaryConsult.hashCode(), summaryConsult2.hashCode());
    }

    @Test
    void testToString() {
        String[] fields = {"id", "destinations", "dtFrom", "dtTo", "currency", "summaryAvgFlights"};
        assertTrue(Stream.of(fields).allMatch(summaryConsult.toString()::contains));
    }
}