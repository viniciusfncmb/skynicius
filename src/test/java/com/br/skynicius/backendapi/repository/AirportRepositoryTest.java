package com.br.skynicius.backendapi.repository;

import com.br.skynicius.backendapi.model.entity.Airport;
import com.br.skynicius.backendapi.model.entity.faker.AirportFaker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
class AirportRepositoryTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AirportRepository airportRepository;

    @Test
    void findAirportByIata() {
        Airport airport = AirportFaker.build();
        airport.setId(null);
        entityManager.persist(airport);
        entityManager.flush();

        String iataCode = airport.getIata();
        Optional<Airport> found = airportRepository.findAirportByIata(iataCode);
        assertNotNull(found.get());
        assertEquals(airport.getIata(), found.get().getIata());

    }
}