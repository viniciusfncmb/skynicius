package com.br.skynicius.backendapi.controller;

import com.br.skynicius.backendapi.service.FlightService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FlightController.class)
@ActiveProfiles("test")
class FlightControllerTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FlightService flightService;

    @Test
    void getSummaryFlight() throws Exception {

        LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        requestParams.add("fly_to", "OPO,LIS");
        requestParams.add("date_from", "2020/01/01");
        requestParams.add("date_to", "2020/01/30");

        mvc.perform(post("/flight/avg")
            .accept(MediaType.APPLICATION_JSON)
            .params(requestParams))
            .andExpect(status().isOk());
    }
}