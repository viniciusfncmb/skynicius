package com.br.skynicius.backendapi.controller;

import com.br.skynicius.backendapi.service.SummaryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SummaryController.class)
@ActiveProfiles("test")
class SummaryControllerTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SummaryService service;

    @Test
    void findAll() throws Exception {
        mvc.perform(get("/summary")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void deleteAll() throws Exception {
        mvc.perform(delete("/summary")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}