package com.br.skynicius.backendapi.service;

import com.br.skynicius.backendapi.exception.BusinessException;
import com.br.skynicius.backendapi.model.dto.FlightRequestDto;
import com.br.skynicius.backendapi.model.dto.FlightResponseDto;
import com.br.skynicius.backendapi.model.dto.SummaryFlightDto;
import com.br.skynicius.backendapi.model.entity.Airport;
import com.br.skynicius.backendapi.model.entity.SummaryAvgFlight;
import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.br.skynicius.backendapi.model.entity.faker.AirportFaker;
import com.br.skynicius.backendapi.repository.AirportRepository;
import com.br.skynicius.backendapi.repository.SummaryConsultRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.internal.function.numeric.Sum;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest
class FlightServiceTest {

    @Autowired
    private FlightService flightService;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    AirportRepository airportRep;

    @Autowired
    SummaryConsultRepository summaryConsultRep;

    @TestConfiguration
    static class TestConfig {
        @Bean
        public FlightService flightService(
                RestTemplate restTemplate,
                AirportRepository airportRepository,
                SummaryConsultRepository summaryConsultRep
        ) {
            return new FlightServiceImpl(restTemplate, airportRepository, summaryConsultRep);
        }
    }

    @Test
    void getSummaryFlight() throws IOException, BusinessException {
        ObjectMapper mapper = new ObjectMapper();

        Resource resource = new ClassPathResource("skypickerResp.json");
        FlightResponseDto respDto = mapper.readValue(resource.getInputStream(), FlightResponseDto.class);

        Airport airport = AirportFaker.build();
        airport.setIata("OPO");
        airportRep.save(airport);

        Mockito
                .when(restTemplate.getForObject(
                        ArgumentMatchers.anyString(), ArgumentMatchers.any()))
                .thenReturn(respDto);

        FlightRequestDto reqParamDto = new FlightRequestDto("OPO", "EUR", LocalDate.now(), LocalDate.now());
        SummaryConsult summaryConsult = flightService.getSummaryFlight(reqParamDto);

        Iterable<SummaryConsult> summaries = summaryConsultRep.findAll();
        List<SummaryConsult> list = IterableUtils.toList(summaries);
        assertFalse(list.isEmpty());

        SummaryConsult found = summaries.iterator().next();
        assertEquals(found, summaryConsult);

        assertEquals(
                airport.getName(),
                summaryConsult.getSummaryAvgFlights().stream()
                        .filter(x -> "OPO".equalsIgnoreCase(x.getAirportCode()))
                        .map(SummaryAvgFlight::getAirportName)
                        .reduce("", String::concat)
        );

    }

    @Test
    void testBusinessException() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Resource resource = new ClassPathResource("skypickerResp.json");
        FlightResponseDto respDto = mapper.readValue(resource.getInputStream(), FlightResponseDto.class);
        respDto.setData(Collections.emptyList());

        Mockito
                .when(restTemplate.getForObject(
                        ArgumentMatchers.anyString(), ArgumentMatchers.any()))
                .thenReturn(respDto);

        FlightRequestDto reqParamDto = new FlightRequestDto("OPO", "EUR", LocalDate.now(), LocalDate.now());

        Exception exception = assertThrows(BusinessException.class, () -> {
            flightService.getSummaryFlight(reqParamDto);
        });

        String expectedMessage = "he Skypicker's API didn't return data for the reported parameters.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));


    }
}