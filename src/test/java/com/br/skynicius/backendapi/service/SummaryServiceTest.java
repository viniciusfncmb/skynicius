package com.br.skynicius.backendapi.service;

import com.br.skynicius.backendapi.model.entity.SummaryConsult;
import com.br.skynicius.backendapi.model.entity.faker.SummaryConsultFaker;
import com.br.skynicius.backendapi.repository.SummaryConsultRepository;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.event.annotation.BeforeTestExecution;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest
class SummaryServiceTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private SummaryConsultRepository repository;

    @Autowired
    private SummaryService summaryService;

    @Profile("test")
    @TestConfiguration
    static class TestConfig {
        @Bean
        public SummaryService summaryService(SummaryConsultRepository repository) {
            return new SummaryServiceImpl(repository);
        }
    }

    @BeforeEach
    public void init(){
        SummaryConsult summaryConsult = SummaryConsultFaker.build();
        repository.save(summaryConsult);
        summaryConsult = SummaryConsultFaker.build();
        repository.save(summaryConsult);
    }

    @Test
    void findAll() {
        Iterable<SummaryConsult> summaries = summaryService.findAll();
        List<SummaryConsult> list = IterableUtils.toList(summaries);
        assertFalse(list.isEmpty());
        assertEquals(2, list.size());
    }

    @Test
    void deleteALl() {
        repository.deleteAll();
        Iterable<SummaryConsult> summaries = summaryService.findAll();
        List<SummaryConsult> list = IterableUtils.toList(summaries);
        assertTrue(list.isEmpty());
    }
}