# Skynicius
Challenge COCUS.

## Documentation
https://skynicius.herokuapp.com/swagger-ui.html


## Main Paths
- **/flight/avg -**  *Returns average flight prices (Verb: post)*
- **/summary -**  *Find all flight summaries (Verb: get)*
- **/summary -** *Delete all flight summaries - (Verb: delete)*

## External Path
- https://api.skypicker.com/flights

## Set Up
- Running Main Class: *BackendapiApplication.java*

### Entities
- Airport
- SummaryAvgFlight
- SummaryConsult

## Main Technologies

- Java 14
- Spring Boot 2.3
    - Spring Cache
    - Spring Data Rest
    - Spring Actuator
- PostgresSQL
- Swagger 3.0.0
- JUnit 5
- Maven
- Lombok

### Spring Profiles
* dev
* heroku
